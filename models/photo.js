var mongoose = require('mongoose');
var Schema2 = mongoose.Schema;

var PhotoSchema = new Schema2({
    user_id: String,
    name: String,
    bitmap: String,
    media_data : String,
    media_display: String
}, {
    versionKey : false
});


module.exports = mongoose.model('photo', PhotoSchema);