var mongoose = require('mongoose');
var Schema1 = mongoose.Schema;

var contactSchema = new Schema1({
    name: String,
    number: String,
    user_id: String
}, {
    versionKey: false
});


module.exports = mongoose.model('contact', contactSchema);