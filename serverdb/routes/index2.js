module.exports = function(app, Photo){
    
    //////////////// Esential function ///////////////////

    //Get all photos
    app.get('/api/photos', function(req, res){
        Photo.find(function(err, photos){
            if(err){
                return res.status(500).send({error: 'database failure'});
            }
            res.json(photos);
        })
    });

    //Get photos by unique id
    app.get('/api/photos/user_id/:user_id', function(req, res){
        Photo.find({user_id: req.params.user_id}, function(err, photos){
            if(err) return res.status(500).json({error: err});
            if(photos.length == 0) return res.status(404).json({error: 'photo not found'});
            res.json(photos);
        })
    });


    //Backup photos
    app.post('/api/photos/backup', function(req, res){
    Photo.remove({}, function(err){
        if(err) return res.json({error: " We cannot delete all documents"});
    });
    for(var i = 0; i < req.body.length; i++){
        new_json = req.body[i];
        var photo = new Photo();
        photo.user_id = new_json.user_id;
        photo.name = new_json.name;
        photo.bitmap = new_json.bitmap;
        photo.media_data = new_json.media_data;
        photo.media_display = new_json.media_display;

        photo.save(function(err){
            if(err) {
                console.error(err);
                res.json({result: 'error'});
                return;
            }
            })
            if(i == req.body.length - 1){
                res.json({result: 'Multiple document inserted'});
            }
    }
});
}





