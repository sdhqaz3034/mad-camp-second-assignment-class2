module.exports = function(app, Contact){
    //Get single contact
    app.get('/api/contacts/:contact_id', function(req, res){
        Contact.findOne({_id: req.params.contact_id}, function(err, contact){
            if(err) return res.status(500).json({error: err});
            if(!contact) return res.status(404).json({error: 'contact not found'});
            res.json(contact);
        })
    });

    //Get contact by name
    app.get('/api/contacts/name/:name', function(req, res){
        Contact.find({name: req.params.name}, function(err, contacts){
            if(err) return res.status(500).json({error: err});
            if(contacts.length == 0) return res.status(404).json({error: 'contact not found'});
            res.json(contacts);
        })
    });

    //Get contact by number
    app.get('/api/contacts/number/:number', function(req, res){
        Contact.find({number: req.params.number}, function(err, contacts){
            if(err) return res.status(500).json({error: err});
            if(contacts.length == 0) return res.status(404).json({error: 'contact not found'});
            res.json(contacts);
        })
    });


    // //Create contact only one
    // app.post('/api/contacts/one', function(req, res){
    //     var contact = new Contact();
    //     contact.name = req.body.name;
    //     contact.number = req.body.number;

    //     contact.save(function(err){
    //         if(err) {
    //             console.error(err);
    //             res.json({result: 'error'});
    //             return;
    //         }
    //         res.json({result: 'created'});
    //     });
    // });



    //Upload contact many or all
    app.post('/api/contacts/many', function(req, res){
        var contact_array = [];
        for(var i = 0; i < req.body.length; i++){
            new_json = req.body[i];
            var contact = new Contact();
            contact.name = new_json.name;
            contact.number = new_json.number;
            //contact.userid = new.json.userid;
            contact.save(function(err){
            if(err) {
                console.error(err);
                res.json({result: 'error'});
                return;
            }
            })
            if(i == req.body.length - 1){
                res.json({result: 'Multiple document inserted'});
            }
        }
    });


    //Update the contact
    app.put('/api/contacts/:contact_id', function(req, res){
        Contact.findById(req.params.contact_id, function(err, contact){
            if(err) return res.status(500).json({ error : 'database failure'});
            if(!contact) return res.status(404).json({error: 'contact not found'});

            if(req.body.name) contact.name = req.body.name;
            if(req.body.number) contact.number = req.body.number;

            contact.save(function(err){
                if(err) res.status(500).json({error: 'failed to update'});
                res.json({mesage: 'contact updated'});
            });
        });
    });

    //Delete contact
    app.delete('/api/contacts/:contact_id', function(req, res){
        Contact.remove({_id: req.params.contact_id}, function(err, output){
            if(err) return res.status(500).json({error: "contact not found"});
            res.json({message: 'contact deleted'});
            res.status(204).end();
        })
    });



    //////////////// Esential function ///////////////////

    //Get all contacts
    app.get('/api/contacts', function(req, res){
        Contact.find(function(err, contacts){
            if(err){
                return res.status(500).send({error: 'database failure'});
            }
            res.json(contacts);
        })
    });


    //Get contacts by unique id
    app.get('/api/contacts/user_id/:user_id', function(req, res){
        Contact.find({user_id: req.params.user_id}, function(err, contacts){
            if(err) return res.status(500).json({error: err});
            if(contacts.length == 0) return res.status(404).json({error: 'contact not found'});
            res.json(contacts);
        })
    });



    //Backup contacts
    app.post('/api/contacts/backup', function(req, res){
    Contact.deleteMany({}, function(err){
        if(err) return res.json({error: " We cannot delete all documents"});
    });
    var contact_array = [];
    for(var i = 0; i < req.body.length; i++){
        new_json = req.body[i];
        var contact = new Contact();
        contact.name = new_json.name;
        contact.number = new_json.number;
        contact.user_id = new_json.user_id;
        contact.save(function(err){
            if(err) {
                console.error(err);
                res.json({result: 'error'});
                return;
            }
            })
            if(i == req.body.length - 1){
                res.json({result: 'Multiple document inserted'});
            }
    }
});
}





