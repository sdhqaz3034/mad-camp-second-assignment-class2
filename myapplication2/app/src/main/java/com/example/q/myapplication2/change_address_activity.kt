package com.example.q.myapplication2


import android.content.ContentUris
import android.content.ContentValues
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.support.constraint.ConstraintLayout
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.Toast


class change_address_activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.changing_address)
        setTitle("연락처 수정")

        var ok_button = findViewById(R.id.save) as Button
        var cancel_button = findViewById(R.id.cancel) as Button
        var name_receiver : EditText = findViewById(R.id.nameText) as EditText
        var phone_number_receiver : EditText = findViewById(R.id.phonenumber) as EditText
        var background = findViewById(R.id.background) as ConstraintLayout
        var intent = intent
        var name = intent.extras.getString("name") as String
        var phone_number = intent.extras.getString("phone_number") as String
        name_receiver.setText(name)
        phone_number_receiver.setText(phone_number)

        background.setOnClickListener({
            var imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(name_receiver.getWindowToken(), 0)
            imm.hideSoftInputFromWindow(phone_number_receiver.getWindowToken(), 0)
        })

        //확인 버튼 눌렀을 때 행동
        ok_button.setOnClickListener({
            // 이름 정보
            val new_name = name_receiver.text.toString()
            //전화번호 정보
            val new_phone_number = phone_number_receiver.text.toString()

            //이름 미입력 시 유저에게 입력하라고 알림
            if(new_name == null || name.isEmpty()) {
                val t: Toast = Toast.makeText(this, "이름이 입력되지 않았습니다.", Toast.LENGTH_SHORT)
                t.show()
            }
            else {
                //전화번호 미입력 시 유저에게 입력하라고 알림
                if (new_phone_number == null || phone_number.isEmpty()) {
                    val t: Toast = Toast.makeText(this, "상대방의 전화번호가 입력되지 않았습니다.", Toast.LENGTH_SHORT)
                    t.show()
                }
                // DB 접근하여 연락처 추가
                else{
                    delete_dialog(name, new_name, new_phone_number)
                }
            }
        })
        // 취소 버튼 누를 시 그냥 종료
        cancel_button.setOnClickListener({
            this.finish()
        })
    }

    private fun delete_dialog(old_name: String, new_name:String, phone_number:String) {
        val ad = AlertDialog.Builder(this)
        ad.setTitle("정말로 변경하시겠습니까?")       // 제목 설정
        ad.setMessage("변경하시면 되돌릴 수 없습니다.")   // 내용 설정

        // 취소 버튼 설정
        ad.setNegativeButton("취소") { dialog, which ->
            dialog.dismiss()     //닫기
        }
        // 확인 버튼 설정
        ad.setPositiveButton("변경") { dialog, which ->

            val cr = contentResolver
            val cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null)
            if (cur.moveToFirst()) {
                do {
                    val target_name = cur.getString(13)
                    val lookupKey = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY))
                    val uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, lookupKey)
                    println("The uri is " + uri.toString())
                    if(target_name == old_name){
                        cr.delete(uri, null, null)
                        dialog.dismiss()
                    }
                } while (cur.moveToNext())
            }
            val contentValues = ContentValues()
            contentValues.put(ContactsContract.RawContacts.CONTACT_ID, 0)
            contentValues.put(
                ContactsContract.RawContacts.AGGREGATION_MODE,
                ContactsContract.RawContacts.AGGREGATION_MODE_DISABLED
            )
            val rawContactUri = contentResolver.insert(ContactsContract.RawContacts.CONTENT_URI, contentValues)
            val rawContactId = ContentUris.parseId(rawContactUri)

            contentValues.clear()
            contentValues.put(ContactsContract.Data.RAW_CONTACT_ID, rawContactId)
            contentValues.put(
                ContactsContract.Data.MIMETYPE,
                ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE
            )
            contentValues.put(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
            contentValues.put(ContactsContract.CommonDataKinds.Phone.NUMBER, phone_number)
            var dataUri = contentResolver.insert(ContactsContract.Data.CONTENT_URI, contentValues)

            contentValues.clear();
            contentValues.put(ContactsContract.Data.RAW_CONTACT_ID, rawContactId);
            contentValues.put(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE);
            contentValues.put(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, new_name);
            dataUri = getContentResolver().insert(ContactsContract.Data.CONTENT_URI, contentValues);

            //저장 완료 시 저장되었습니다 문구와 함께 종료
            val complete: Toast = Toast.makeText(this, "변경되었습니다.", Toast.LENGTH_SHORT)
            complete.show()
            finish()
        }
        // 창 띄우기
        ad.show()
    }

}