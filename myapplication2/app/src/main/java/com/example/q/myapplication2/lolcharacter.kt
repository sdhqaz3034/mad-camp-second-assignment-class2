package com.example.q.myapplication2


import android.graphics.Bitmap
import android.graphics.Canvas

class lolcharacter(image_bitmap:Bitmap, identifier_num: Int, start_x: Float, start_y: Float, window_x: Int, window_y: Int, full_hp_bar:Bitmap, empty_hp_bar: Bitmap, degree: Float, rotation_degree: Int){
    var myimage_bitmap=image_bitmap
    var my_x = start_x
    var my_y = start_y
    var my_bullets = mutableListOf<lolcharacter>()
    var cw = 0          // 캐릭터의 폭/2
    var ch = 0        // 캐릭터의 높이/2
    var HP = 0
    var hp_bar_full = full_hp_bar
    var hp_bar_empty = empty_hp_bar
    var degree = degree
    var MAX_HP = 5
    var rotation_degree = rotation_degree

    var window_x = window_x
    var window_y = window_y


    init {
        if(identifier_num == 0){//메인 케릭터를 의미
            myimage_bitmap = Bitmap.createScaledBitmap(image_bitmap, window_x/5, window_y/10,false)
            cw = myimage_bitmap.getWidth()
            ch = myimage_bitmap.getHeight()
            HP = MAX_HP
        }

        else if(identifier_num==1){// 아군 총알 의미
            myimage_bitmap = Bitmap.createScaledBitmap(image_bitmap, 100,100,false)
            cw=100
            ch=100
            HP=1
        }

        else if(identifier_num == 2){//적 케릭터를 의미
            myimage_bitmap = Bitmap.createScaledBitmap(image_bitmap, window_x/5, window_y/10,false)
            cw = myimage_bitmap.getWidth()
            ch = myimage_bitmap.getHeight()
            HP=MAX_HP
        }

        else if(identifier_num==3){// 아군 총알 의미
            myimage_bitmap = Bitmap.createScaledBitmap(image_bitmap, 100,100,false)
            cw=100
            ch=100
            HP=1
        }

        else if(identifier_num==4){// 보스 캐릭터를 의미
            myimage_bitmap = Bitmap.createScaledBitmap(image_bitmap, window_x/10, window_y/5,false)
            cw=cw/10
            ch=ch/10
            MAX_HP = 10
            HP = MAX_HP

        }
        hp_bar_full = Bitmap.createScaledBitmap(full_hp_bar, window_x/60, ch,false)
        hp_bar_empty = Bitmap.createScaledBitmap(empty_hp_bar, window_x/60, ch,false)
    }

    fun draw(canvas: Canvas){
        canvas.drawBitmap(myimage_bitmap, my_x , my_y, null)
    }



    fun my_bullets_flying(){
        var deleted_num = 0
        for (z in 0 until my_bullets.size) {
            var current_bullet = my_bullets[z-deleted_num]
            var bullet_degree = current_bullet.degree
            if (current_bullet.move_right(30*Math.cos(bullet_degree/180*Math.PI))) {
                my_bullets.removeAt(z-deleted_num)
                deleted_num++
                break
            }
            if(bullet_degree>0){
                if(current_bullet.move_down(-30*Math.sin(bullet_degree/180*Math.PI))){
                    my_bullets.removeAt(z-deleted_num)
                    deleted_num++
                }
            }
            else{
                if(current_bullet.move_up(30*Math.sin(bullet_degree/180*Math.PI))){
                    my_bullets.removeAt(z-deleted_num)
                    deleted_num++
                }
            }
        }
    }

    fun enemy_bullets_flying(){
        var deleted_num = 0
        for (z in 0 until my_bullets.size) {
            var current_bullet = my_bullets[z-deleted_num]
            var bullet_degree = current_bullet.degree
            if (current_bullet.move_left(30*Math.cos(bullet_degree/180*Math.PI))) {
                my_bullets.removeAt(z-deleted_num)
                deleted_num++
                break
            }
            if(bullet_degree>0){
                if(current_bullet.move_down(30*Math.sin(bullet_degree/180*Math.PI))){
                    my_bullets.removeAt(z-deleted_num)
                    deleted_num++
                }
            }
            else{
                if(current_bullet.move_up(-30*Math.sin(bullet_degree/180*Math.PI))){
                    my_bullets.removeAt(z-deleted_num)
                    deleted_num++
                }
            }
        }
    }

    fun shoot(bitmap: Bitmap, start_degree: Float, rotation_degree: Int){
        my_bullets.add( lolcharacter(bitmap, 1, my_x+cw*2/3, my_y+ch/2/2, window_x, window_y, hp_bar_full, hp_bar_empty, start_degree, rotation_degree))
    }

    fun shoot_enemy(bitmap: Bitmap, start_degree: Float, rotation_degree: Int){
        my_bullets.add( lolcharacter(bitmap, 3, my_x+cw*2/3, my_y+ch/2,  window_x, window_y, hp_bar_full, hp_bar_empty, start_degree, rotation_degree))
    }

    fun draw_bullet(canvas: Canvas){
        for(i in 0 until my_bullets.size){
            var current_bullet = my_bullets[i]
            if(rotation_degree==1)
                draw_with_degree_rotation(
                    current_bullet.myimage_bitmap,
                    current_bullet.my_x,
                    current_bullet.my_y,
                    current_bullet.my_x,
                    current_bullet.my_y,
                    canvas,
                    current_bullet.degree + 120F,
                    rotation_degree)
            else
                draw_with_degree_rotation(
                    current_bullet.myimage_bitmap,
                    current_bullet.my_x,
                    current_bullet.my_y,
                    current_bullet.my_x,
                    current_bullet.my_y,
                    canvas,
                    current_bullet.degree-30F,
                    rotation_degree)
        }
    }

    fun bullet_check(enemy_bullet:MutableList<lolcharacter>){
        var number_bombed_bullet = 0
        var number_of_bullets = enemy_bullet.size
        for(i in 0 until number_of_bullets){
            val k = i-number_bombed_bullet
            val current_bullet = enemy_bullet[k]
            val bullet_x = current_bullet.my_x
            val bullet_y = current_bullet.my_y
            val bullet_end_x = bullet_x + current_bullet.cw
            val bullet_end_y = bullet_y + current_bullet.ch
            //bullet이 내 캐릭터 안에 포함되는지 체크
            if((bullet_x  >= my_x && bullet_x <= my_x + cw) || (bullet_end_x >= my_x && bullet_end_x <= my_x + cw)){
                if((bullet_y >= my_y && bullet_y <= my_y + ch) || (bullet_end_y >= my_y && bullet_end_y <= my_y + ch)){
                    HP--
                    number_bombed_bullet++
                    enemy_bullet.removeAt(k)
                }
            }
            if(HP == 0)
                hp_bar_full = Bitmap.createScaledBitmap(hp_bar_full, window_x/60, 1,false)
            else
                hp_bar_full = Bitmap.createScaledBitmap(hp_bar_full, window_x/60, ch*HP/MAX_HP,false)
        }
    }

    fun move_up(distance: Double): Boolean {
        val new_y = my_y-distance
        if(new_y>0) {
            my_y = new_y.toFloat()
            return false
        }
        else {
            my_y = 0.toFloat()
            return true
        }
    }


    fun move_down(distance: Double): Boolean {
        var new_y = my_y+distance
        var max_y = window_y - ch
        if(new_y < max_y) {
            my_y = new_y.toFloat()
            return false
        }
        else {
            my_y = max_y.toFloat()
            return true
        }
    }

    fun move_left(distance: Double): Boolean {
        var new_x = my_x-distance
        if(new_x>0){
            my_x = new_x.toFloat()
        return false
        }
        else {
            my_x = 0.toFloat()
            return true
        }
    }

    fun move_right(distance: Double): Boolean {
        var new_x = my_x+distance
        var max_x = window_x - cw
        if(new_x<max_x){
            my_x = new_x.toFloat()
        return false
       }
        else{
            my_x = max_x.toFloat()
        return true
        }
    }
}