package com.example.q.myapplication2


import android.graphics.BitmapFactory
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.widget.*
import com.github.chrisbanes.photoview.PhotoView
import android.widget.Toast
import android.content.ContentValues
import android.content.Intent

class One_picture : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.one_picture_show)
        val fileURL = intent.getStringExtra("fileURL")
        var bitmap = BitmapFactory.decodeFile(fileURL)
        val oneContent = findViewById(R.id.oneContent) as PhotoView

        oneContent.setImageBitmap(bitmap)

        val fileName = intent.getStringExtra("fileName")
        val oneContentText = findViewById<TextView>(R.id.oneContentText)
        oneContentText.text = fileName
        setTitle(fileName)

        val fix_button = findViewById<Button>(R.id.picturename_fix)
        fix_button.setOnClickListener {
            val ad = AlertDialog.Builder(this)
            ad.setTitle("변경할 이름을 적어주십시오.")
            val et = EditText(this)
            ad.setView(et)
            et.setText(fileName)

            // 취소 버튼 설정
            ad.setNegativeButton("저장하지 않는다") { dialog, which ->
                dialog.dismiss()     //닫기
            }
            // 확인 버튼 설정
            ad.setPositiveButton("저장") { dialog, which ->
                val value = et.text.toString()
                if (false) {
                    Toast.makeText(this, "빈 이름으로는 저장 할 수 없습니다.", Toast.LENGTH_SHORT).show()
                } else {
                    val fileID = intent.getStringExtra("fileID")
                    val mUpdateValues = ContentValues()
                    mUpdateValues.put(MediaStore.Images.Media.DISPLAY_NAME, value)
                    println("value : $value")
                    var k = contentResolver.update(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,mUpdateValues,"_id = $fileID",null)
                    println(" k : $k")
                    oneContentText.text = value
                }

            }
            ad.show()
        }

        val delete_button = findViewById<Button>(R.id.picturename_delete)
        delete_button.setOnClickListener {
            val ad = AlertDialog.Builder(this)
            ad.setTitle("삭제하시겠습니까?")
            // 취소 버튼 설정
            ad.setNegativeButton("취소") { dialog, which ->
                dialog.dismiss()     //닫기
            }
            // 확인 버튼 설정
            ad.setPositiveButton("삭제") { dialog, which ->
                val fileID = intent.getStringExtra("fileID")
                val mUpdateValues = ContentValues()
                //mUpdateValues.put(MediaStore.Images.Media.DISPLAY_NAME, value)
                var k = contentResolver.delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,"_id = $fileID",null)
                print(k)
                var resultIntent = Intent()
                resultIntent.putExtra("return", 1)
                setResult(1)
                finish()
            }
            ad.show()
        }

    }

}