package com.example.q.myapplication2


import android.app.*
import android.content.*
import android.graphics.*
import android.os.*
import android.view.*
import android.view.MotionEvent
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import java.util.*
import com.github.nkzawa.socketio.client.IO
import com.github.nkzawa.socketio.client.Socket
import java.net.URISyntaxException
import com.github.nkzawa.emitter.Emitter

class lol_private_activity : Activity() {
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val mSocket: Socket
        run {
            try {
                mSocket = IO.socket("http://socrip4.kaist.ac.kr:1480")
                mSocket.connect()
            } catch (e: URISyntaxException) {
                e.printStackTrace()
            }
        }


        // 전체 화면 사용하기
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(GameView(this))
    }

    //-----------------------------------
    //       Game View
    //-----------------------------------
    internal inner class GameView//-----------------------------------
    //   Constructor - 게임 초기화
    //-----------------------------------
        (context: Context) : View(context) {                          // 화면의 폭과 높이
        var dx: Float = 0.toFloat()
        var dy: Float = 0.toFloat()                                        // 캐릭터가 이동할 방향과 거리
        var character : lolcharacter? = null
        var cw= 0
        var ch= 0               // 캐릭터의 폭과 높이

        //각종 이미지


        var sideinversion = Matrix()
        var backimage = BitmapFactory.decodeResource(resources, R.drawable.background)!!
        var characterimage = BitmapFactory.decodeResource(resources, R.drawable.ezreal)!!
        var mirror_images = mutableListOf<Bitmap>()
        var victoryimage = BitmapFactory.decodeResource(resources, R.drawable.victory)!!
        var defeatimage = BitmapFactory.decodeResource(resources, R.drawable.defeat)!!
        var enemyimage = BitmapFactory.decodeResource(resources, R.drawable.zed)!!
        var bulletimage1 = BitmapFactory.decodeResource(resources, R.drawable.ezq)!!
        var bulletimage2 = BitmapFactory.decodeResource(resources, R.drawable.zedq)!!
        var right_button = BitmapFactory.decodeResource(resources, R.drawable.right_button)!!
        var left_button = BitmapFactory.decodeResource(resources, R.drawable.left_button)!!
        var q_button = BitmapFactory.decodeResource(resources, R.drawable.ezrealqbutton)!!
        var e_button = BitmapFactory.decodeResource(resources, R.drawable.ezrealebutton)!!
        var one_second_left = BitmapFactory.decodeResource(resources, R.drawable.circle1)!!
        var two_second_left = BitmapFactory.decodeResource(resources, R.drawable.circle2)!!
        var three_second_left = BitmapFactory.decodeResource(resources, R.drawable.circle3)!!
        var four_second_left = BitmapFactory.decodeResource(resources, R.drawable.circle4)!!
        var five_second_left = BitmapFactory.decodeResource(resources, R.drawable.circle5)!!
        var ez_e_effect = BitmapFactory.decodeResource(resources, R.drawable.ez_e_effect)!!
        var empty_hp_bar = BitmapFactory.decodeResource(resources, R.drawable.hp_bar_empty)!!
        var full_hp_bar = BitmapFactory.decodeResource(resources, R.drawable.hp_bar_full)!!
        var skill_direction = BitmapFactory.decodeResource(resources, R.drawable.attack_direction)

        var count = 0                                                             //카운트
        var real_width = 0                                                        //전체 화면의 넓이
        var real_height = 0                                                       //전체 화면의 높이
        var enemy_list = mutableListOf<lolcharacter>()  //적들의 정보가 저장되는 곳
        var movex = mutableListOf<Int>()                        //터치 스크린 이벤트의 x좌표 입력 받는 리스트
        var movey = mutableListOf<Int>()                        //터치 스크린 이벤트의 y좌표 입력 받는 리스트
        var move_direction = 1                                                    //적이 움직일 방향 1은 세로 화면 상 위로 움직임 0은 아래로 움직임
        var qcooltime = 0                                                          //내 q스킬 쿨타임
        var ecooltime = 0                                                          //내 e스킬 쿨타임
        var canRun = true                                                         //게임이 실행중인가? 체크
        var character_direction = 0                                               //내 캐릭터의 이미지 방향
        var enemy_direction = 0                                                   //적 캐릭터의 이미지 방향
        var degree = 0F                                                           //각도
        var rotation_degree = 0                                                   //각도 선회 횟수

        var q_button_y = 0F
        var e_button_y = 0F
        var skill_button_size = 0

        var past_x = 0F
        var past_y = 0F

        var global_num = arrayOfNulls<Int>(2)
        var global_x = arrayOfNulls<Int>(2)
        var global_y = arrayOfNulls<Int>(2)


        //------------------------------------
        //      Timer Handler
        //------------------------------------
        var mHandler: Handler = object : Handler() {               // 타이머로 사용할 Handler
            override fun handleMessage(msg: Message) {
                if(canRun == true){
                    invalidate()                                              // onDraw() 다시 실행
                    this.sendEmptyMessageDelayed(0, 10) // 10/1000초마다 실행
                }
                else{
                    var handler = Handler().postDelayed({
                        finish()
                    }, 1000000000)
                }
            }
        } // Handler

        init {
            //핸드폰 화면의 사이즈를 가져온다.
            val display = (context.getSystemService(Context.WINDOW_SERVICE) as WindowManager).defaultDisplay
            real_width = display.width          // 화면의 가로폭
            real_height = display.height        // 화면의 세로폭

            // 배경화면, 승리/패배 시 화면
            backimage = Bitmap.createScaledBitmap(backimage, real_width, real_height,false)
            victoryimage = Bitmap.createScaledBitmap( victoryimage, real_width/2, real_height/2, false)
            defeatimage = Bitmap.createScaledBitmap( defeatimage, real_width/2, real_height/2, false)

            //캐릭터 이미지 설정
            character = lolcharacter(characterimage, 0, real_width/8.toFloat(), real_height/2.toFloat() , real_width, real_height, full_hp_bar, empty_hp_bar, 0F, 0)
            characterimage = character!!.myimage_bitmap
            sideinversion.setScale(1F, -1F)
            mirror_images.add(Bitmap.createBitmap(characterimage, 0, 0,  character!!.cw, character!!.ch, sideinversion, false))
            cw= character!!.myimage_bitmap.getWidth() / 2
            ch= character!!.myimage_bitmap.getHeight()                  // 캐릭터의 폭과 높이
            character!!.my_x = character!!.my_x - cw
            character!!.my_y = character!!.my_y - ch

            // 비전 이동 이펙트와 q스킬 방향키
            ez_e_effect = Bitmap.createScaledBitmap(ez_e_effect, character!!.cw, character!!.ch, false)
            skill_direction = Bitmap.createScaledBitmap(skill_direction, real_width/10, real_height/30, false)

            //오른쪽/왼쪽 방향키
            right_button = Bitmap.createScaledBitmap(right_button, real_width/5, real_height/7, false)
            left_button = Bitmap.createScaledBitmap(left_button, real_width/5, real_height/7, false)


            //스킬 버튼 y위치와 사이즈
            skill_button_size = real_height/10
            q_button_y = (real_height - skill_button_size).toFloat()
            e_button_y = (real_height - 2*skill_button_size).toFloat()
            q_button = Bitmap.createScaledBitmap(q_button, skill_button_size, skill_button_size, false)
            e_button = Bitmap.createScaledBitmap(e_button, skill_button_size, skill_button_size, false)

            //쿨타임 시간 이미지
            one_second_left = Bitmap.createScaledBitmap(one_second_left, skill_button_size, skill_button_size, false)
            two_second_left = Bitmap.createScaledBitmap(two_second_left, skill_button_size, skill_button_size, false)
            three_second_left = Bitmap.createScaledBitmap(three_second_left, skill_button_size, skill_button_size, false)
            four_second_left = Bitmap.createScaledBitmap(four_second_left, skill_button_size, skill_button_size, false)
            five_second_left = Bitmap.createScaledBitmap(five_second_left, skill_button_size, skill_button_size, false)

            make_enemy()

            mHandler.sendEmptyMessageDelayed(0, 10)
        }

        //-----------------------------------
        //       실제 그림을 그리는 부분
        //-----------------------------------
        public override fun onDraw(canvas: Canvas) {
            degree = (count)%150F - 75F
            if(degree == 75F || degree == -75F)
                rotation_degree++
            //이미지 버튼과 왼쪽 오른쪽 버튼 그리기
            canvas.drawBitmap(backimage, 0.toFloat(), 0.toFloat(), null)
            //HP 0되면 게임 종료
            if(character!!.HP == 0){
                canvas.drawBitmap(defeatimage, real_width/4.toFloat(), real_height/4.toFloat(), null)
                character = null
                canRun = false
                return
            }

            character!!.draw(canvas)
            character!!.my_bullets_flying()

            for(i in 0 until enemy_list.size) {
                var zed = enemy_list[i]
                var random_constant = Random().nextInt(50)

                if(random_constant%30==0)
                    move_direction = -move_direction
                if(count%2==0){
                        if(move_direction==1) {
                            zed.move_up(10.0)
                            if(enemy_direction == 1){
                                zed.myimage_bitmap = enemyimage
                                enemy_direction=0
                            }
                        }
                        else{
                            zed.move_down(10.0)
                            if(enemy_direction == 0){
                                zed.myimage_bitmap = mirror_images[1]
                                enemy_direction=1
                            }

                        }
                }

                if(count%200 == 50){
                    if(rotation_degree == 0)
                        zed.shoot_enemy(bulletimage2, degree, rotation_degree)
                    else
                        zed.shoot_enemy(bulletimage2, -degree, rotation_degree)
                }

                //on-hit 판정!!
                zed.bullet_check(character!!.my_bullets)
                character!!.bullet_check((zed.my_bullets))

                if(zed.HP <=0){
                    enemy_list.removeAt(i)
                    canvas.drawBitmap(victoryimage, real_width/4.toFloat(), real_height/4.toFloat(), null)
                    canRun = false
                }
                else{
                    zed.draw(canvas)
                    zed.enemy_bullets_flying()
                    zed.draw_bullet(canvas)
                    enemy_list[i]=zed
                }


                canvas.drawBitmap(zed.hp_bar_empty, (zed.my_x - real_width/30), zed.my_y, null)
                canvas.drawBitmap(zed.hp_bar_full, (zed.my_x - real_width/30), zed.my_y, null)
            }

            character!!.draw_bullet(canvas)
            count++

            //내 캐릭터의 HP바 그리기
            canvas.drawBitmap(character!!.hp_bar_empty, (character!!.my_x + character!!.myimage_bitmap.width), character!!.my_y, null)
            canvas.drawBitmap(character!!.hp_bar_full, (character!!.my_x + character!!.myimage_bitmap.width), character!!.my_y, null)


            //맨 마지막에 버튼을 그려주어야 캐릭터가 버튼 밑에 놓여서 버튼이 가려지지 않음
            draw_basic(canvas)

            draw_with_degree_rotation(
                skill_direction,
                character!!.my_x + character!!.myimage_bitmap.width*2/3,
                character!!.my_y + character!!.myimage_bitmap.height/2,
                character!!.my_x + character!!.myimage_bitmap.width*2/3-real_width/30,
                character!!.my_y + character!!.myimage_bitmap.height/2-real_width/30,
                canvas,
                degree,
                rotation_degree)
        } // onDraw 끝



        // 기본적인 버튼들을 그려주는 함수
        private fun draw_basic(canvas: Canvas){
            canvas.drawBitmap(left_button, 0.toFloat(), 0.toFloat(), null)
            canvas.drawBitmap(right_button, 0.toFloat(), real_height/7.toFloat(), null)
            // 스킬 버튼 그리기
            canvas.drawBitmap(q_button, 0.toFloat(), q_button_y, null)
            canvas.drawBitmap(e_button, 0.toFloat(), e_button_y, null)
            if(qcooltime != 0){
                if(qcooltime <= 100)
                    canvas.drawBitmap(one_second_left, 0.toFloat(), q_button_y, null)
                else if(qcooltime <= 200)
                    canvas.drawBitmap(two_second_left, 0.toFloat(), q_button_y, null)
                qcooltime--
            }

            if(ecooltime != 0){
                if(ecooltime <= 100)
                    canvas.drawBitmap(one_second_left, 0.toFloat(), e_button_y, null)
                else if(ecooltime <= 200)
                    canvas.drawBitmap(two_second_left, 0.toFloat(), e_button_y, null)
                else if(ecooltime <= 300)
                    canvas.drawBitmap(three_second_left, 0.toFloat(), e_button_y, null)
                else if(ecooltime <= 400)
                    canvas.drawBitmap(four_second_left, 0.toFloat(), e_button_y, null)
                else
                    canvas.drawBitmap(five_second_left, 0.toFloat(), e_button_y, null)
                if(ecooltime>470)
                    canvas.drawBitmap(ez_e_effect, past_x, past_y, null)
                ecooltime--
            }
        }

        private fun make_enemy(){
            enemy_list.add(lolcharacter(enemyimage, 2, 4*real_width/5.toFloat(), real_height/2.toFloat(), real_width, real_height, full_hp_bar, empty_hp_bar, 0F, 0))
            enemyimage = enemy_list[0].myimage_bitmap
            mirror_images.add(Bitmap.createBitmap(enemyimage, 0, 0,  character!!.cw, character!!.ch, sideinversion, false))
            movex.add(10)
            movey.add(10)

        }

        fun check_cooltime(index: Int): Boolean {
            println("checking")
            if(index == 0){
                if(qcooltime==0)
                    return true
            }
            else if(index == 1){
                if(ecooltime==0)
                    return true
            }
            println("false")
            return false
        }

        fun set_cooltime(index: Int, max_cooltime: Int){
            println("setting")
            if(index == 0)
                qcooltime = max_cooltime
            else if(index == 1)
                ecooltime = max_cooltime
        }


        //------------------------------------
        //      onTouchEvent
        //------------------------------------
        override fun onTouchEvent(event: MotionEvent): Boolean {
            // 화면 터치 시
            var pointer_count = event.pointerCount
            //동시 3터치 이상은 받아들이지 않는다.
            if(pointer_count >2 ) pointer_count = 2

            //모든 touch들의 x, y좌표를 저장
            when(event.action and MotionEvent.ACTION_MASK){
                MotionEvent.ACTION_DOWN ->{
                    global_num[0] = event.getPointerId(0) //터치한 순간부터 부여되는 포인트 고유번호.
                    global_x[0] = event.x.toInt()
                    global_y[0] = event.y.toInt()

                }
                MotionEvent.ACTION_POINTER_DOWN -> {
                    for (i in 0 until pointer_count) {
                        global_num[i] = event.getPointerId(i) //터치한 순간부터 부여되는 포인트 고유번호.
                        global_x[i] = event.getX(i).toInt()
                        global_y[i] = event.getY(i).toInt()                    }
                }
                MotionEvent.ACTION_MOVE -> {
                    for (i in 0 until pointer_count) {
                        global_num[i] = event.getPointerId(i)
                        global_x[i] = event.getX(i).toInt()
                        global_y[i] = event.getY(i)
                            .toInt()
                    }
                }
            }

            // 모든 touch에 대해 작동 수행
            for(i in 0 until pointer_count){
                //q스킬 버튼 누른다
                if(global_x[i]!! > 0 && global_x[i]!! < real_height/8 && global_y[i]!! > 7*real_height/8 && global_y[i]!! < real_height) {
                    if(check_cooltime(0)){
                        set_cooltime(0,200)
                        if(rotation_degree%2 == 0)
                            character!!.shoot(bulletimage1, degree, rotation_degree)
                        else
                            character!!.shoot(bulletimage1, -degree, rotation_degree)
                        println("sex")
                    }
                }
                //e스킬 버튼 누른다
                else if(pointer_count == 2 && global_x[i]!! > 0 && global_x[i]!! < real_height/8 && global_y[i]!! > 6*real_height/8 && global_y[i]!! < 7*real_height/8){
                    //왼쪽을 누르고 있다면 왼쪽으로 비전 이동
                    if(check_cooltime(1)){
                        if(global_x[1-i]!! < real_width/5 && global_y[1-i]!! < real_height/7) {
                            //비전 이동 전 위치 저장
                            past_x = character!!.my_x
                            past_y = character!!.my_y
                            //이동
                            character!!.move_up(400.0)
                            if(character_direction == 0){
                                character!!.myimage_bitmap = mirror_images[0]
                                character_direction=1
                            }
                            set_cooltime(1,500)
                        }
                        //오른쪽 누르고 있다면 오른쪽으로 비전 이동
                        else if(global_x[1-i]!! < real_width/5 && global_y[1-i]!! < 2*real_height/7 && global_y[1-i]!! >= real_height/7) {
                            //비전 이동 전 위치 저장
                            past_x = character!!.my_x
                            past_y = character!!.my_y
                            //이동
                            character!!.move_down(400.0)
                            if(character_direction == 1) {
                                character!!.myimage_bitmap = characterimage
                                character_direction = 0
                            }
                            set_cooltime(1,500)
                        }
                    }
                }
                //왼쪽 버튼 누르면 화면상에서 위로 감(가로 모드니깐)
                else if(global_x[i]!! < real_width/5 && global_y[i]!! < real_height/7) {
                    character!!.move_up(10.0)
                    if(character_direction == 0){
                        character!!.myimage_bitmap = mirror_images[0]
                        character_direction=1
                    }
                }
                //오른쪽 버튼 누르면 화면상에서 아래로 감(가로 모드니깐)
                else if(global_x[i]!! < real_width/5 && global_y[i]!! < 2*real_height/7 && global_y[i]!! >= real_height/7) {
                    character!!.move_down(10.0)
                    if(character_direction == 1) {
                        character!!.myimage_bitmap = characterimage
                        character_direction = 0
                    }
                }
            }
            // 화면 터치 상태로 이동
            return true
        } // onTouchEvent
    } // GameView 끝
} // 프로그램 끝