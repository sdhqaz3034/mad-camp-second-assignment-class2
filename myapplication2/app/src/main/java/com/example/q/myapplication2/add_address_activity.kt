package com.example.q.myapplication2


import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.content.ContentUris
import android.provider.ContactsContract
import android.content.ContentValues
import android.content.Context
import android.provider.ContactsContract.CommonDataKinds.Phone
import android.view.inputmethod.InputMethodManager
import android.widget.*
import android.support.constraint.ConstraintLayout


class add_address_activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.adding_address)
        setTitle("연락처 추가")

        var ok_button = findViewById(R.id.save) as Button
        var cancel_button = findViewById(R.id.cancel) as Button
        var name_receiver : EditText = findViewById(R.id.nameText) as EditText
        var phone_number_receiver : EditText = findViewById(R.id.phonenumber) as EditText
        var background = findViewById(R.id.background) as ConstraintLayout


        //키보드 올라와 있을 때 빈 공간 터치 시 키보드 내리기
        background.setOnClickListener({
            var imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(name_receiver.getWindowToken(), 0)
            imm.hideSoftInputFromWindow(phone_number_receiver.getWindowToken(), 0)
        })

        //확인 버튼 눌렀을 때 행동
        ok_button.setOnClickListener({
            // 이름 정보
            val name = name_receiver.text.toString()
            //전화번호 정보
            val phone_number = phone_number_receiver.text.toString()

            //이름 미입력 시 유저에게 입력하라고 알림
            if(name == null || name.isEmpty()) {
                val t: Toast = Toast.makeText(this, "이름이 입력되지 않았습니다. ", Toast.LENGTH_SHORT)
                t.show()
            }
            else {
                //전화번호 미입력 시 유저에게 입력하라고 알림
                if (phone_number == null || phone_number.isEmpty()) {
                    val t: Toast = Toast.makeText(this, "상대방의 전화번호가 입력되지 않았습니다. ", Toast.LENGTH_SHORT)
                    t.show()
                }
                // DB 접근하여 연락처 추가
                else{
                    val contentValues = ContentValues()
                    contentValues.put(ContactsContract.RawContacts.CONTACT_ID, 0)
                    contentValues.put(
                        ContactsContract.RawContacts.AGGREGATION_MODE,
                        ContactsContract.RawContacts.AGGREGATION_MODE_DISABLED
                    )
                    val rawContactUri = contentResolver.insert(ContactsContract.RawContacts.CONTENT_URI, contentValues)
                    val rawContactId = ContentUris.parseId(rawContactUri)

                    contentValues.clear()
                    contentValues.put(ContactsContract.Data.RAW_CONTACT_ID, rawContactId)
                    contentValues.put(
                        ContactsContract.Data.MIMETYPE,
                        ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE
                    )
                    contentValues.put(ContactsContract.CommonDataKinds.Phone.TYPE, Phone.TYPE_MOBILE)
                    contentValues.put(ContactsContract.CommonDataKinds.Phone.NUMBER, phone_number)
                    var dataUri = contentResolver.insert(ContactsContract.Data.CONTENT_URI, contentValues)

                    contentValues.clear();
                    contentValues.put(ContactsContract.Data.RAW_CONTACT_ID, rawContactId);
                    contentValues.put(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE);
                    contentValues.put(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, name);
                    dataUri = getContentResolver().insert(ContactsContract.Data.CONTENT_URI, contentValues);

                    //저장 완료 시 저장되었습니다 문구와 함께 종료
                    val complete: Toast = Toast.makeText(this, "추가되었습니다. ", Toast.LENGTH_SHORT)
                    complete.show()
                    this.finish()
                }
            }
        })
        // 취소 버튼 누를 시 그냥 종료
        cancel_button.setOnClickListener({
            this.finish()
        })
    }
}