package com.example.q.myapplication2

import android.content.Intent
import android.media.Image
import android.media.session.MediaSession
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.Toast
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.login.LoginManager
import com.facebook.login.widget.LoginButton


class Login : AppCompatActivity() {
    var callbackManager = CallbackManager.Factory.create()
    var loginCallback = LoginCallback(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login)

        var a = AccessToken.getCurrentAccessToken()
        if(a!=null){
            if(a.isExpired==false){
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }
            else{
                val t: Toast = Toast.makeText(this, "로그인 세션이 만료되었습니다.\n다시 로그인해주세요.", Toast.LENGTH_SHORT)
                t.show()
            }
        }

        var facebook_loginButton = findViewById(R.id.login_button) as LoginButton
        var login_button = findViewById(R.id.btn_custom_login) as ImageButton
        var logout_button = findViewById(R.id.btn_custom_logout) as ImageButton

        facebook_loginButton.setReadPermissions("email")
        facebook_loginButton.setReadPermissions("public_profile")
        facebook_loginButton.registerCallback(callbackManager, loginCallback)

        login_button.setOnClickListener(View.OnClickListener {
            if(a==null){
                facebook_loginButton.performClick()
            }
            else if(a.isExpired==true){
                val t: Toast = Toast.makeText(this, "로그인 세션이 만료되었습니다.\n먼저 로그아웃해주세요.", Toast.LENGTH_SHORT)
                t.show()
            }
        })

        logout_button.setOnClickListener(View.OnClickListener {
            var loginManager = LoginManager.getInstance()
            loginManager.logOut()
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
        /*
        var a = AccessToken.getCurrentAccessToken()
        if(a!=null){
            var userid = a.userId
*/
            val show_information = Intent(this, MainActivity::class.java)
            show_information.putExtra("user_id", "sdhqaz")
            startActivity(show_information)
        //}
    }
}