package com.example.q.myapplication2

import android.Manifest
import android.content.pm.PackageManager
import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import com.facebook.AccessToken
import com.jcraft.jsch.*


class ConnectServer : AppCompatActivity() {
    var REQUIRED_CONNECT = 0
    var REQUIRED_CONNECT2 = 0
    var REQUIRED_CONNECT3 = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.connecting)

        title = AccessToken.ACCESS_TOKEN_KEY
        println(title)

        var logout_button = findViewById(R.id.btn_custom_logout) as Button

        val permissionEX_Storage = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.INTERNET)
        val permissionEX_Storage2 = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.ACCESS_NETWORK_STATE)
        val permissionEX_Storage3 = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.ACCESS_WIFI_STATE)

        if (permissionEX_Storage == PackageManager.PERMISSION_DENIED) {
            REQUIRED_CONNECT = 1
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.INTERNET), 1)
        }
        else{
            if (permissionEX_Storage2 == PackageManager.PERMISSION_DENIED) {
                REQUIRED_CONNECT2 = 1
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_NETWORK_STATE), 1)
            }
            else {
                if (permissionEX_Storage3 == PackageManager.PERMISSION_DENIED) {
                    REQUIRED_CONNECT3 = 1
                    ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_WIFI_STATE), 1)
                }
                else {
                    SshTask().execute()
                }
            }
        }
    }

    class SshTask : AsyncTask<Void, Void, String>() {
        override fun doInBackground(vararg p0: Void?): String {
            val output = executeRemoteCommand("root", "kaistCS496^", "143.248.140.106")
            print(output)
            return output.toString()
        }
    }
}

fun executeRemoteCommand(username: String,
                         password: String,
                         hostname: String,
                         port: Int = 1922) {
    println("==> Connecting to $hostname")
    var session: Session? = null
    var channel: Channel? = null

    // 2. 세션 객체를 생성한다 (사용자 이름, 접속할 호스트, 포트를 인자로 준다.)
    try {
        // 1. JSch 객체를 생성한다.
        val jsch = JSch()
        session = jsch.getSession(username, hostname, port)

        // 3. 패스워드를 설정한다.
        session!!.setPassword(password)

        // 4. 세션과 관련된 정보를 설정한다.
        val config = java.util.Properties()
        // 4-1. 호스트 정보를 검사하지 않는다.
        config["StrictHostKeyChecking"] = "no"
        session!!.setConfig(config)

        // 5. 접속한다.
        session!!.connect()

        // 6. sftp 채널을 연다.
        channel = session!!.openChannel("exec")

        // 8. 채널을 SSH용 채널 객체로 캐스팅한다
        val channelExec = channel as ChannelExec?

        println("==> Connected to $hostname")

        channelExec!!.setCommand("touch /test/jschTest.txt")
        channelExec.connect()

        println("==> Connected to $hostname")

    } catch (e: JSchException) {
        e.printStackTrace()
    } finally {
        if (channel != null) {
            channel!!.disconnect()
        }
        if (session != null) {
            session!!.disconnect()
        }
    }
}