package com.example.q.myapplication2


import android.content.Context
import android.graphics.BitmapFactory
import android.view.View
import android.widget.BaseAdapter
import android.view.ViewGroup
import android.widget.ImageView

class galleryAdapter(val context: Context, val Photodatas : ArrayList<PhoneGallery>, val width : Int , val height : Int) : BaseAdapter() {

    var mContext : Context? = context
    var mPhotodatas : ArrayList<PhoneGallery>? = Photodatas
    val options = BitmapFactory.Options()

    override fun getCount(): Int {
        return mPhotodatas!!.size
    }

    override fun getItem(position : Int): String {
        return mPhotodatas!![position].mediaData
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {

        options.inSampleSize = 1
        var imageView : ImageView
        imageView = if(convertView == null){
            ImageView(mContext)
        }else{
            convertView as ImageView
        }
        //이미지뷰에 주어진 위치의 이미지를 설정함

        /*
        if(mPhotodatas!![position].mediaSize.toInt() > 500000){
            options.inSampleSize = 4
        }else if(mPhotodatas!![position].mediaSize.toInt() > 250000){
            options.inSampleSize = 2
        }else {
            options.inSampleSize = 1
        }

        */
        //var bitmap : Bitmap = BitmapFactory.decodeFile(mPhotodatas!![position].mediaData, options)
        //var bitmap = MediaStore.Images.Thumbnails.getThumbnail(mContentResolver,mPhotodatas!![position].mediaID.toLong(),MediaStore.Images.Thumbnails.MINI_KIND,null)
        //bitmap = Bitmap.createScaledBitmap(bitmap, width/3,width/3,false)
        imageView!!.setImageBitmap(mPhotodatas!![position].bitmap)
        return imageView

        /*
        var imageView : ImageView? = ImageView(mContext)
        imageView!!.setImageBitmap(mPhotodatas!![position].bitmap)
        return imageView!!
        */
    }

}