package com.example.q.myapplication2


import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast


class address_information : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.address_information)

        var name_text = findViewById(R.id.name_text) as TextView
        var phone_number_text = findViewById(R.id.phone_number) as TextView
        var change_button = findViewById(R.id.change) as Button
        var delete_button = findViewById(R.id.delete) as Button


        var intent = intent
        var name = intent.extras.getString("name") as String
        var phone_number = intent.extras.getString("phone_number") as String
        name_text.setText(name)
        phone_number_text.setText(phone_number)
        setTitle(name)

        change_button.setOnClickListener(View.OnClickListener {
            var permissionaddress_w =
                ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.WRITE_CONTACTS)
            if (permissionaddress_w == PackageManager.PERMISSION_DENIED) {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_CONTACTS), 1)
            } else {
                val change_addressActivity = Intent(this, change_address_activity::class.java)
                change_addressActivity.putExtra("name", name)
                change_addressActivity.putExtra("phone_number", phone_number)
                startActivity(change_addressActivity)
                finish()
            }
        })

        delete_button.setOnClickListener(View.OnClickListener {
            var permissionaddress_w =
                ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.WRITE_CONTACTS)
            if (permissionaddress_w == PackageManager.PERMISSION_DENIED) {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_CONTACTS), 1)
            } else {
                delete_dialog(name)
            }
        })


    }

    private fun delete_dialog(name: String) {
        val ad = AlertDialog.Builder(this)
        ad.setTitle("정말로 삭제하시겠습니까?")       // 제목 설정
        ad.setMessage("삭제하시면 되돌릴 수 없습니다.")   // 내용 설정

        // 취소 버튼 설정
        ad.setNegativeButton("취소") { dialog, which ->
            dialog.dismiss()     //닫기
        }
        // 확인 버튼 설정
        ad.setPositiveButton("삭제확인") { dialog, which ->

            val cr = contentResolver
            val cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null)
            if (cur.moveToFirst()) {
                do {
                    val target_name = cur.getString(13)
                    val lookupKey = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.LOOKUP_KEY))
                    val uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, lookupKey)
                    println("The uri is " + uri.toString())
                    if(target_name == name){
                        cr.delete(uri, null, null)
                        val t: Toast = Toast.makeText(this, "삭제되었습니다.", Toast.LENGTH_SHORT)
                        t.show()
                        dialog.dismiss()
                        finish()
                    }
                } while (cur.moveToNext())
            }
        }
        // 창 띄우기
        ad.show()
    }
}