package com.example.q.myapplication2


import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Point
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.Button
import android.app.WallpaperManager
import android.media.Image
import android.support.v7.app.AlertDialog
import android.widget.ImageButton
import android.widget.ImageView
import com.facebook.login.LoginManager


class MainActivity : AppCompatActivity() {

    var REQUIRED_CONNECT = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var userid = intent.extras.getString("user_id") as String
        var button1= findViewById(R.id.button1) as ImageButton
        var button2= findViewById(R.id.button2) as ImageButton
        var button3= findViewById(R.id.button3) as ImageButton
        var logout_button = findViewById(R.id.btn_custom_logout) as ImageButton
        val view = findViewById(R.id.background) as ImageView
        view.setImageDrawable(WallpaperManager.getInstance(this).drawable)

        button1.setOnClickListener(View.OnClickListener {
            var permissionaddress = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.READ_CONTACTS)
            if (permissionaddress == PackageManager.PERMISSION_DENIED) {
                REQUIRED_CONNECT = 1
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_CONTACTS), 1)
            }else{
                var next_activity = Intent(this, address_activity::class.java)
                next_activity.putExtra("user_id", userid)
                startActivity(next_activity)
            }
        })

        button2.setOnClickListener(View.OnClickListener {
            println("clicked")
            val permissionEX_Storage = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            if (permissionEX_Storage == PackageManager.PERMISSION_DENIED) {
                REQUIRED_CONNECT = 2
                println("wwwwwww")
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)
            }
            else {
                println("pass")
                var connectserver = Intent(this, gallery_activity::class.java)
                var a= window.windowManager.defaultDisplay
                var b = Point()
                a.getSize(b)
                connectserver.putExtra("width", b.x)
                connectserver.putExtra("height", b.y)
                connectserver.putExtra("user_id", userid)
                startActivity(connectserver)
            }
        })
        button3.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this, online_waiting_room::class.java))
        })

        logout_button.setOnClickListener(View.OnClickListener {
            logout_dialog()
        })
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        var PERMISSION_REQUEST_CODE = 100
        var check_result = true
        // 모든 퍼미션을 허용했는지 체크합니다.
        if(requestCode == 1){
            for (result in grantResults) {
                println("result : $result")
                if (result != PackageManager.PERMISSION_GRANTED) {
                    check_result = false
                    break
                }
            }
            if (check_result) {
                // 모든 퍼미션을 허용했다면 REQUIRED_CONNECTED 에 따라서 해결한다.
                if(REQUIRED_CONNECT== 1){
                    startActivity(Intent(this, address_activity::class.java))
                }else if(REQUIRED_CONNECT==2){
                    var connectserver = Intent(this, gallery_activity::class.java)
                    var a= window.windowManager.defaultDisplay
                    var b = Point()
                    a.getSize(b)
                    connectserver.putExtra("width", b.x)
                    connectserver.putExtra("width", b.y)
                    startActivity(connectserver)
                }
            }
        }
    }

    private fun logout_dialog() {
        val ad = AlertDialog.Builder(this)
        ad.setTitle("로그아웃하시겠습니까?")       // 제목 설정
        ad.setMessage("로그인 화면으로 돌아갑니다.")   // 내용 설정
        ad.setNegativeButton("취소") { dialog, which ->
            dialog.dismiss()     //닫기
        }
        // 확인 버튼 설정
        ad.setPositiveButton("확인") { dialog, which ->
            var loginManager = LoginManager.getInstance()
            loginManager.logOut()
            startActivity(Intent(this, Login::class.java))
        }
        ad.show()
    }
}
