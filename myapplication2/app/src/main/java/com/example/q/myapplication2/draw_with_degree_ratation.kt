package com.example.q.myapplication2

import android.graphics.Bitmap
import android.graphics.Canvas

fun draw_with_degree_rotation(image: Bitmap, rotate_x: Float, rotate_y: Float, set_x: Float, set_y: Float, canvas: Canvas, deg:Float, rotation_deg: Int){
    canvas.save()
    if(rotation_deg%2 == 0)
        canvas.rotate(-deg, rotate_x, rotate_y)
    else
        canvas.rotate(deg, rotate_x, rotate_y)
    canvas.drawBitmap(image, set_x, set_y, null)
    canvas.restore()
}