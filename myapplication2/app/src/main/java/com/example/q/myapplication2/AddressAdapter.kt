package com.example.q.myapplication2


import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class AddressAdapter(val context: Context, val phoneList : ArrayList<PhoneAddress>) : RecyclerView.Adapter<AddressAdapter.Holder>(){

    inner class Holder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
        val recycletext1 = itemView?.findViewById(R.id.recycletext1) as TextView
        val recycletext2 = itemView?.findViewById(R.id.recycletext2) as TextView

        fun bind (phone: PhoneAddress, context: Context): Boolean {
            recycletext1?.text = phone.PhoneNumber
            recycletext2?.text = phone.name
            itemView.setOnLongClickListener(View.OnLongClickListener {
                val show_information = Intent(context, address_information::class.java)
                show_information.putExtra("name", recycletext1.text.toString())
                show_information.putExtra("phone_number", recycletext2.text.toString())
                show_information.putExtra("phone_id", phone.id)
                context.startActivity(show_information)
                return@OnLongClickListener true
            })
            return true
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(context).inflate(R.layout.recyclerview_item, p0, false)
        return Holder(view)
    }

    override fun getItemCount(): Int {
        return phoneList.size
    }

    override fun onBindViewHolder(p0: Holder, position: Int) {
        p0?.bind(phoneList[position], context)
    }

}