package com.example.q.myapplication2


import android.Manifest
import android.content.*
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.RemoteException
import android.provider.ContactsContract
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.address.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.util.ArrayList


class address_activity : AppCompatActivity()  {

    val phoneArray_as_JSON = JSONArray()
    var userid = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.address)
        var intent1 = intent
        userid = intent1.extras.getString("user_id") as String
        setTitle("연락처")
        DrawPicture()

        var adding_button= findViewById(R.id.add_button) as Button
        var download_button = findViewById(R.id.download) as Button
        var upload_button = findViewById(R.id.upload) as Button

        adding_button.setOnClickListener(View.OnClickListener {
            var permissionaddress_w = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.WRITE_CONTACTS)
            if (permissionaddress_w == PackageManager.PERMISSION_DENIED) {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_CONTACTS), 1)
            }else {
                val add_addressActivity = Intent(this, add_address_activity::class.java)
                startActivity(add_addressActivity)
            }
        })

        upload_button.setOnClickListener(View.OnClickListener {
            var permissionaddress_i = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.INTERNET)
            if (permissionaddress_i == PackageManager.PERMISSION_DENIED) {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.INTERNET), 1)
            }else {
                val uploadContact = object : Thread() {
                    override fun run() {
                        try {
                            //Create a URL object
                            var url: URL? = null
                            url = URL("http://socrip4.kaist.ac.kr:1480/api/contacts/backup")
                            //Create a connection
                            val connection = url.openConnection() as HttpURLConnection
                            connection.requestMethod = "POST"
                            connection.doOutput = true     //true: POST, PUT, false : GET
                            connection.doInput = true
                            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8")
                            connection.setRequestProperty(
                                "Accept",
                                "application/json"
                            )  //To be sure not to run into encoding problems, you should specify the encoding, if it is not UTF-8:

                            val os = connection.outputStream
                            os.write(phoneArray_as_JSON.toString().toByteArray(charset("UTF-8")))
                            os.close()

                            val `is` = connection.inputStream
                            val InputReader = InputStreamReader(`is`, "UTF-8")
                            val BufferReader = BufferedReader(InputReader)
                            `is`.close()

                        } catch (e: MalformedURLException) {
                            e.printStackTrace()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }

                    }
                }
                uploadContact.start()
                Toast.makeText(this, "연락처가 업로드 되었습니다", Toast.LENGTH_SHORT).show()
            }
        })
        download_button.setOnClickListener(View.OnClickListener {
            var permissionaddress_i = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.INTERNET)
            if (permissionaddress_i == PackageManager.PERMISSION_DENIED) {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.INTERNET), 1)
            }else {
                val downloadContact = object : Thread() {
                    override fun run() {
                        try {
                            while(!phoneArray_as_JSON.isNull(0)){
                                phoneArray_as_JSON.remove(0)
                            }

                            contentResolver.delete(ContactsContract.RawContacts.CONTENT_URI, ContactsContract.RawContacts.CONTACT_ID, null)

                            //Create a URL object
                            var url: URL? = null
                            url = URL("http://socrip4.kaist.ac.kr:1480/api/contacts/user_id/${userid}")
                            //Create a connection
                            val connection = url!!.openConnection() as HttpURLConnection
                            connection.requestMethod = "GET"
                            connection.doOutput = false     //true: POST, PUT, false : GET
                            connection.doInput = true
                            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8")
                            connection.setRequestProperty(
                                "Accept",
                                "application/json"
                            )  //To be sure not to run into encoding problems, you should specify the encoding, if it is not UTF-8:

                            val `is` = connection.inputStream
                            val InputReader = InputStreamReader(`is`, "UTF-8")
                            val bufferedReader = BufferedReader(InputReader)
                            if (bufferedReader != null) {
                                //3-2 response 읽고 jsonarray만들기
                                val inputArray = JSONArray(bufferedReader.readLine())

                                for (i in 0 until inputArray.length()) {
                                    val inputItem = inputArray.get(i) as JSONObject
                                    val inputName = inputItem.get("name") as String
                                    val inputNum = inputItem.get("number") as String
                                    val inputid = inputItem.get("user_id") as String
                                    phoneArray_as_JSON.put(inputItem)
                                }

                                //3-3 jsonarray 하나씩 보며 저장하기
                                for (i in 0 until inputArray.length()) {
                                    val inputItem = inputArray.get(i) as JSONObject
                                    val inputName = inputItem.get("name") as String
                                    val inputNum = inputItem.get("number") as String
                                    val contentValues = ContentValues()
                                    contentValues.put(ContactsContract.RawContacts.CONTACT_ID, 0)
                                    contentValues.put(
                                        ContactsContract.RawContacts.AGGREGATION_MODE,
                                        ContactsContract.RawContacts.AGGREGATION_MODE_DISABLED
                                    )
                                    val rawContactUri =
                                        contentResolver.insert(ContactsContract.RawContacts.CONTENT_URI, contentValues)
                                    val rawContactId = ContentUris.parseId(rawContactUri)

                                    contentValues.clear()
                                    contentValues.put(ContactsContract.Data.RAW_CONTACT_ID, rawContactId)
                                    contentValues.put(
                                        ContactsContract.Data.MIMETYPE,
                                        ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE
                                    )
                                    contentValues.put(
                                        ContactsContract.CommonDataKinds.Phone.TYPE,
                                        ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE
                                    )
                                    contentValues.put(ContactsContract.CommonDataKinds.Phone.NUMBER, inputNum)
                                    var dataUri =
                                        contentResolver.insert(ContactsContract.Data.CONTENT_URI, contentValues)

                                    contentValues.clear();
                                    contentValues.put(ContactsContract.Data.RAW_CONTACT_ID, rawContactId);
                                    contentValues.put(
                                        ContactsContract.Data.MIMETYPE,
                                        ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE
                                    );
                                    contentValues.put(
                                        ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME,
                                        inputName
                                    );
                                    dataUri = getContentResolver().insert(
                                        ContactsContract.Data.CONTENT_URI,
                                        contentValues
                                    );
                                }
                            }
                        } catch (e: MalformedURLException) {
                            e.printStackTrace()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        } catch (e: RemoteException) {
                            e.printStackTrace()
                        } catch (e: OperationApplicationException) {
                            e.printStackTrace()
                        }
                    }
                }
                downloadContact.start()
                Toast.makeText(this, "연락처가 업데이트되었습니다.", Toast.LENGTH_SHORT).show()
                var next_activity = Intent(this, address_activity::class.java)
                next_activity.putExtra("user_id", userid)
                startActivity(next_activity)
                finish()
            }
        })
    }

    override fun onRestart() {
        super.onRestart()
        DrawPicture()
    }

    fun DrawPicture(){
        while(!phoneArray_as_JSON.isNull(0)){
            phoneArray_as_JSON.remove(0)
        }
        var phoneList = arrayListOf<PhoneAddress>()
        var uri : Uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI
        val projection = arrayOf(
            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, // 연락처 이름
            ContactsContract.CommonDataKinds.Phone.NUMBER, // 연락처
            ContactsContract.CommonDataKinds.Phone._ID
        )
        val sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC"
        val contactCursor = contentResolver.query(uri, projection, null, null, sortOrder)
        if (contactCursor.moveToFirst()) {
            do {

                var phonename = contactCursor.getString(0)
                println(phonename)
                var phonenumber = contactCursor.getString(1)
                var phoneid = contactCursor.getString(2)
                var a = PhoneAddress(name = phonenumber, PhoneNumber = phonename, id = phoneid)
                phoneList.add(a)
                val phoneObject = JSONObject()
                try {
                    phoneObject.put("name", phonename)
                    phoneObject.put("number", phonenumber)
                    phoneObject.put("user_id", userid);
                    phoneArray_as_JSON.put(phoneObject)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            } while (contactCursor.moveToNext())



            val mAdapter = AddressAdapter(this, phoneList)
            view1.adapter = mAdapter

            val lm = LinearLayoutManager(this)
            view1.layoutManager = lm
            view1.setHasFixedSize(true)
        }
    }
}
