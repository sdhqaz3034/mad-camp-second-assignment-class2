package com.example.q.myapplication2

import android.Manifest
import android.content.*
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.provider.MediaStore
import kotlinx.android.synthetic.main.gallery.*
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.RemoteException
import android.provider.ContactsContract
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.AdapterView
import android.view.MotionEvent
import android.widget.Button
import android.widget.Toast
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.util.*

class gallery_activity : AppCompatActivity()  {

    var PhotoList = arrayListOf<PhoneGallery>()

    val phoneArray_as_JSON = JSONArray()
    var userid = ""

    var width = 50
    var height = 100
    var page = 1
    var distance = 0.0F
    var x_Start = 0.0F
    var y_Start = 0.0F
    var x_Change = 0.0F
    var y_Change = 0.0F
    var changeActivity = 0
    var endpage = 0
    var onepage = 30
    var lastPage = 0;
    var renewal = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        width = intent.getIntExtra("width",1000)
        height = intent.getIntExtra("height",2000)
        page = intent.getIntExtra("page",1)
        userid = intent.getStringExtra("user_id")
        setContentView(R.layout.gallery)
        initializing()
        upload_from_device()
        drawFunction()

        var download_button = findViewById(R.id.download) as Button
        var upload_button = findViewById(R.id.upload) as Button

        upload_button.setOnClickListener(View.OnClickListener {
            var permissionaddress_i = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.INTERNET)
            if (permissionaddress_i == PackageManager.PERMISSION_DENIED) {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.INTERNET), 1)
            }else {
                val uploadContact = object : Thread() {
                    override fun run() {
                        try {
                            //Create a URL object
                            var url: URL? = null
                            url = URL("http://socrip4.kaist.ac.kr:1480/api/photos/backup ")
                            //Create a connection
                            val connection = url.openConnection() as HttpURLConnection
                            connection.requestMethod = "POST"
                            connection.doOutput = true     //true: POST, PUT, false : GET
                            connection.doInput = true
                            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8")
                            connection.setRequestProperty(
                                "Accept",
                                "application/json"
                            )  //To be sure not to run into encoding problems, you should specify the encoding, if it is not UTF-8:

                            val os = connection.outputStream
                            os.write(phoneArray_as_JSON.toString().toByteArray(charset("UTF-8")))
                            os.close()

                            val `is` = connection.inputStream
                            val InputReader = InputStreamReader(`is`, "UTF-8")
                            val BufferReader = BufferedReader(InputReader)
                            `is`.close()

                        } catch (e: MalformedURLException) {
                            e.printStackTrace()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }

                    }
                }
                uploadContact.start()
                Toast.makeText(this, "갤러리가 업로드 되었습니다", Toast.LENGTH_SHORT).show()
            }
        })

        download_button.setOnClickListener(View.OnClickListener {
            var permissionaddress_i = ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.INTERNET)
            if (permissionaddress_i == PackageManager.PERMISSION_DENIED) {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.INTERNET), 1)
            }else {
                val downloadContact = object : Thread() {
                    override fun run() {
                        try {
                            while(!phoneArray_as_JSON.isNull(0)){
                                phoneArray_as_JSON.remove(0)
                            }

                            contentResolver.delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,null,null)

                            //Create a URL object
                            var url: URL? = null
                            url = URL("http://socrip4.kaist.ac.kr:1480/api/photos/user_id/${userid}")
                            //Create a connection
                            val connection = url!!.openConnection() as HttpURLConnection
                            connection.requestMethod = "GET"
                            connection.doOutput = false     //true: POST, PUT, false : GET
                            connection.doInput = true
                            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8")
                            connection.setRequestProperty(
                                "Accept",
                                "application/json"
                            )  //To be sure not to run into encoding problems, you should specify the encoding, if it is not UTF-8:

                            val `is` = connection.inputStream
                            val InputReader = InputStreamReader(`is`, "UTF-8")
                            val bufferedReader = BufferedReader(InputReader)
                            if (bufferedReader != null) {
                                //3-2 response 읽고 jsonarray만들기
                                val inputArray = JSONArray(bufferedReader.readLine())
                                for (i in 0 until inputArray.length()) {
                                    val inputItem = inputArray.get(i) as JSONObject
                                    val inputName = inputItem.get("name") as String
                                    val inputBitmap = inputItem.get("bitmap") as String
                                    val inputdisplay = inputItem.get("media_display") as String
                                    val inputdata = inputItem.get("media_data") as String
                                    val inputid = inputItem.get("user_id") as String
                                }

                                //3-3 jsonarray 하나씩 보며 저장하기
                                for (i in 0 until inputArray.length()) {
                                    val inputItem = inputArray.get(i) as JSONObject
                                    val inputName = inputItem.get("name") as String
                                    val inputBitmap = inputItem.get("bitmap") as String
                                    val inputdisplay = inputItem.get("media_display") as String
                                    val inputdata = inputItem.get("media_data") as String
                                    val realBitmap = getBitmapString(inputBitmap) as Bitmap
                                    val contentValues = ContentValues()
                                    val x = MediaStore.Images.Media.insertImage(contentResolver, realBitmap, inputName, inputdisplay)

                                }
                            }
                        } catch (e: MalformedURLException) {
                            e.printStackTrace()
                        } catch (e: IOException) {
                            e.printStackTrace()
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        } catch (e: RemoteException) {
                            e.printStackTrace()
                        } catch (e: OperationApplicationException) {
                            e.printStackTrace()
                        }
                    }
                }
                downloadContact.start()
                Toast.makeText(this, "갤러리가 업데이트되었습니다.", Toast.LENGTH_SHORT).show()
                startActivity(this.intent)
                finish()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        renewal = resultCode
    }

    override fun onResume() {
        super.onResume()
        if(renewal==1){
            drawFunction()
        }
        renewal = 0
    }
    fun initializing(){
        while(!phoneArray_as_JSON.isNull(0)){
            phoneArray_as_JSON.remove(0)
        }
        while(!PhotoList.isEmpty()){
            PhotoList.removeAt(0)
        }
    }

    fun upload_from_device(){

        var firstPage = 0 + onepage*(page-1)
        var secondPage = onepage
        val projection = arrayOf(
            MediaStore.Images.Media._ID,
            MediaStore.Images.Media.DATA,
            MediaStore.Images.Media.DISPLAY_NAME,
            MediaStore.Images.Media.SIZE
        )
        val sortOrder = "_display_name DESC LIMIT "+ firstPage.toString() + ", " + secondPage.toString()
        val imageCursor = contentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, null, sortOrder)
        if (imageCursor.moveToFirst()) {
            do {
                var mediaID =  imageCursor.getString(0)
                var mediaData = imageCursor.getString(1)
                var mediaDisplay = imageCursor.getString(2)
                //var bitmap : Bitmap = BitmapFactory.decodeFile(mediaData,options)
                //var rebitmap = Bitmap.createScaledBitmap(bitmap, width/3,width/3,false)
                var bitmap = MediaStore.Images.Thumbnails.getThumbnail(contentResolver,mediaID.toLong(),MediaStore.Images.Thumbnails.MINI_KIND,null)
                bitmap = Bitmap.createScaledBitmap(bitmap, width/3,width/3,false)
                /*
                if (bitmap.getWidth() >= bitmap.getHeight()){
                    bitmap = Bitmap.createBitmap(bitmap, bitmap.getWidth()/2 - bitmap.getHeight()/2, 0, bitmap.getHeight(), bitmap.getHeight())
                }else{
                    bitmap = Bitmap.createBitmap(bitmap, 0, bitmap.getHeight()/2 - bitmap.getWidth()/2, bitmap.getWidth(), bitmap.getWidth())
                }
                */
                var a = PhoneGallery(mediaID = mediaID, mediaData= mediaData, mediaDisplay = mediaDisplay, bitmap = bitmap)
                PhotoList.add(a)

                var bitmap_str = getBase64String(bitmap)
                val phoneObject = JSONObject()
                try {
                    phoneObject.put("name", mediaID)
                    phoneObject.put("media_data", mediaData)
                    phoneObject.put("media_display", mediaDisplay)
                    phoneObject.put("bitmap", bitmap_str)
                    phoneObject.put("user_id", userid);
                    phoneArray_as_JSON.put(phoneObject)
                } catch (e: JSONException) {
                    e.printStackTrace()
                }

                /*
                     println("ID : " + mediaID + "  Data : " + mediaData + " mediaDisplay : " + mediaDisplay + " media size : " + mediaSize)
                */
            } while (imageCursor.moveToNext())
        }
    }

    fun drawFunction(){

        var firstPage = 0 + onepage*(page-1)
        var secondPage = onepage

        val projection = arrayOf(
            MediaStore.Images.Media._ID,
            MediaStore.Images.Media.DATA,
            MediaStore.Images.Media.DISPLAY_NAME,
            MediaStore.Images.Media.SIZE
        )

        val totalSize = contentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection, null, null, null)
        endpage = totalSize.count
        if( firstPage + onepage >= endpage ){
            setTitle("My Gallery " + (firstPage + 1) + " - " + endpage + " / " + endpage)
            lastPage = 1
        }else{
            setTitle("My Gallery " + (firstPage + 1) + " - " + ( (firstPage) + onepage ) + " / " + endpage)
        }

        val mAdapter = galleryAdapter(this, PhotoList, width, height)
        gallerygrid.adapter = mAdapter
        val onePictureActivity = Intent(this, One_picture::class.java)
        gallerygrid.onItemClickListener = object : AdapterView.OnItemClickListener {
            override fun onItemClick(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                if(changeActivity == 0){
                    onePictureActivity.putExtra("fileID",PhotoList[id.toInt()].mediaID)
                    onePictureActivity.putExtra("fileURL", PhotoList[id.toInt()].mediaData)
                    onePictureActivity.putExtra("fileName", PhotoList[id.toInt()].mediaDisplay)
                    startActivityForResult(onePictureActivity,0)
                }
            }
        }
        gallerygrid.setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN ->{
                    x_Start = event.getX()
                    y_Start = event.getY()
                }
                MotionEvent.ACTION_MOVE->{

                }
                MotionEvent.ACTION_UP -> {
                    x_Change = event.getX() - x_Start
                    y_Change = event.getY() - y_Start
                    if(y_Change > -200 && y_Change < 200){
                        if(x_Change > 85 || x_Change < -85){
                            if(x_Change > 85){
                                changeActivity = 1
                                if(page > 1){
                                    val galleryActivity = Intent(this, gallery_activity::class.java)
                                    galleryActivity.putExtra("width", width)
                                    galleryActivity.putExtra("height", height)
                                    galleryActivity.putExtra("page", page-1)
                                    finish()
                                    startActivity(galleryActivity)
                                    overridePendingTransition(android.R.anim.slide_in_left, R.anim.hold)
                                }
                            }else {
                                if(lastPage == 0){
                                    val galleryActivity = Intent(this, gallery_activity::class.java)
                                    galleryActivity.putExtra("width", width)
                                    galleryActivity.putExtra("height", height)
                                    galleryActivity.putExtra("page", page + 1)
                                    finish()
                                    startActivity(galleryActivity)
                                }
                            }
                            return@setOnTouchListener true
                        }
                    }
                }
            }
            super.onTouchEvent(event)
        }
    }

    fun getBase64String(bitmap: Bitmap): String {
        var byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 1, byteArrayOutputStream)
        var imageByte = byteArrayOutputStream.toByteArray()
        return Base64.getEncoder().encodeToString(imageByte)
    }

    fun getBitmapString(string: String): Bitmap? {
        var decodedByteArray = Base64.getDecoder().decode(string)
        var Bitmap = BitmapFactory.decodeByteArray(decodedByteArray, 0, decodedByteArray.size)
        return Bitmap
    }
}
