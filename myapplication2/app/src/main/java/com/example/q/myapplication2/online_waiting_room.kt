package com.example.q.myapplication2

import android.app.Activity
import android.content.Context
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager


class online_waiting_room : Activity() {
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        setContentView(GameView(this))
    }
    internal inner class GameView
        (context: Context) : View(context) {
        var background = BitmapFactory.decodeResource(resources, R.drawable.menu_background)

    }
}