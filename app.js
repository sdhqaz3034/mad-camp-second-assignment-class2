//Load packages
var express = require('express');
var http = require('http');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var server = http.createServer(app);
var io = require('socket.io').listen(server);

const JSON = require('circular-json');
var router = express.Router();

//Configure app to use bodyParser
app.use(bodyParser.json({limit: '50mb'})); // for parsing application/json
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true })); // for parsing application/x-www-form-urlencoded

//Connect to mongodb server
var db = mongoose.connection;

db.on('error', function(){
    console.log('Connection failed!');
});

db.once('open', function() {
    console.log('Connected to mongod server!');
});

/////////////////////////////////////////////////////////////////////////////////////////

var enter = 0;

///////////////index3/////////////////===SOCKET===//////////////////////////////////////
io.sockets.on('connection', function (socket){   //io.sockets.on인지 io.on인지
    console.log('Connect to server');
    

    //real
    socket.on('entrance', function(message){
        console.log('Entrance!!!!!');
        console.log(message);
        console.log(enter);
        enter = enter + message.state;        //안드로이드 앱에서 사용자가 입장했을때 1, 사용자가 나갔을때 -1을 보내도록
        console.log(enter);
        
        if(enter === 2){
            var cooltime = 3;
            console.log('start!');
            setInterval(function() {
                cooltime-- ;
                console.log(cooltime);
                if(enter !== 2){
                    return;
                }
                if(cooltime===0){
                    io.sockets.emit("start", {start: true});
                    return;
                }
            }, 1000);
        }
    })

    socket.on('reset', function (data) {
        countdown = 1000;
        io.sockets.emit('timer', { countdown: countdown });
      });



    socket.on('can_we_game', function(message){
        io.emit('numplayers', {'number_of_players' : enter});
        //io.broadcast.emit('numplayers', {'number_of_players' : enter});
    })


    socket.on('sendinfo', function(message){
        socket.broadcast.emit('getinfo', message);
        //var user_array_1 = [{"user_id" : user_id, "x" : x, "y" : y, "skill" : skill}];
    })

    socket.on('shootinfo', function(message){
        socket.broadcast.emit('getshootinfo', message);
        //var user_array_1 = [{"user_id" : user_id, "x" : x, "y" : y, "skill" : skill}];
    })



    // socket.on('disconnect', function() {
    //     console.log('user has left');
    //     socket.broadcast.emit('userdisconnect', 'user has left');
    // })

});


///////////////////////////////////////////////////////////////////////////////////////




mongoose.connect('mongodb://localhost/mydb', {useNewUrlParser: true});


var Contact = require('./models/contact');
var Photo = require('./models/photo');
var Game = require('./models/game');

var router = require('./routes/index')(app, Contact);   //이것무엇
var router2 = require('./routes/index2')(app, Photo);
var router3 = require('./routes/index3')(io, Game);

server.listen(80, function() {
	console.log('Server is listening on port 80(1422)');
});	